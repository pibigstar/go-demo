# go语言例子

```
├── file
│   ├── main.go
│   └── rw
│       └── rwfile.go
├── http
│   ├── get_post
│   │   ├── get_demo.go
│   │   └── post_demo.go
│   ├── main.go
│   └── server
│       └── helloServer.go
├── mail
│   └── send_email.go
├── net
│   ├── client
│   │   └── talkClient.go
│   ├── client_main.go
│   ├── server
│   │   └── talkServer.go
│   └── server_main.go
├── restful
│   └── main.go
├── singleton
│   ├── README.md
│   ├── go_single.go
│   └── other_single.go
├── sort
│   ├── sort_demo.go
│   └── sort_struct
│       └── sort.go
├── spider
│   └── spider.go
├── tcp
│   ├── README.md
│   ├── client.go
│   └── server.go
├── test.text
├── utils
│   ├── README.md
│   ├── encrypt
│   │   ├── base64.go
│   │   └── md5_util.go
│   ├── errutil
│   │   ├── check_error.go
│   │   └── print_stack.go
│   ├── flag
│   │   └── flag_demo.go
│   ├── json
│   │   └── json_demo.go
│   ├── retry
│   │   └── retry.go
│   ├── time
│   │   └── time_format.go
│   ├── xml
│   │   └── xml_main.go
│   └── zip
│       └── zip_demo.go
└── vendor
        
        
```